import React from 'react';

const Rodape = props => {
    return (
        <footer className='container-fluid text-center'>
            <a href='#myPage' title='To Top'>
                <span className='glyphicon glyphicon-chevron-up'></span>
            </a>
        <p>Created By <a href='http://www.rodrigoviturino.com.br' title='Rodrigo Viturino'>Rodrigo Viturino</a></p>
        </footer>
    );
}

export default Rodape